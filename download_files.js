const fs = require('fs');
//let rawdata = fs.readFileSync('wldcprueba-eventDays.json');
//let eventDays = JSON.parse(rawdata);
const admin = require('firebase-admin');
const serviceAcount = require('./wldc_serviceAcount.json');
const download = require('download-file');
admin.initializeApp({
    credential: admin.credential.cert(serviceAcount),
    databaseURL: "https://wldcprueba.firebaseio.com"
});
// Get a database reference to our posts
const db = admin.database();
const ref = db.ref("/eventDays");

let eventDays = {};
let pathFolder = "./music/";

function getDataFromFirebase(){
    return new Promise((resolve)=>{
        ref.on("value", function(snapshot) {
            Object.assign(eventDays,snapshot.val());
            resolve('Object obtained');
          }, function (errorObject) {
            console.log("The read failed: " + errorObject.code);
          });
    });
}

// function that find and return object given specific key
function getObjects(obj, key, val) {
    var objects = [];
    for (var i in obj) {
        if (!obj.hasOwnProperty(i)) continue;
        if (typeof obj[i] == 'object') {
            objects = objects.concat(getObjects(obj[i], key, val));
        } else
            //if key matches and value matches or if key matches and value is not passed (eliminating the case where key matches but passed value does not)
            if (i == key && obj[i] == val || i == key && val == '') { //
                objects.push(obj);
            } else if (obj[i] == val && key == '') {
                //only add if the object is not already in the array
                if (objects.lastIndexOf(obj) == -1) {
                    objects.push(obj);
                }
            }
    }
    return objects;
}

function downloadFile(musicUrl, musicName){
    return new Promise(resolve=>{
        var options = {
            directory: `${pathFolder}`,
            filename: `${musicName}.mp3`
        }
        download(musicUrl, options, function (err) {
            if (err) throw err
            //console.log("music " + i);
            resolve('Downloaded');
        });
    });
}
async function saveMusic(){
    let dataFromFirebase = await getDataFromFirebase();
    let musicUrls = getObjects(eventDays, 'music', '');
    console.log("EventDays");
    console.log(eventDays);
    for(let i=0; i<musicUrls.length; i++){
        console.log(musicUrls[i].music);
        if(musicUrls[i].music != false){
            var result = await downloadFile(musicUrls[i].music,musicUrls[i].music_name);
            console.log("Music: " + i + " " + result);
        }
        
    }
}
saveMusic();
